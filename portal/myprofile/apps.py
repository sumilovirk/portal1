from django.apps import AppConfig


class MypofileConfig(AppConfig):
    name = "portal.myprofile"
    verbose_name = "Myprofile"
