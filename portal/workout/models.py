from django.db import models

class Proportion(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    weight = models.FloatField()
    neck = models.FloatField()
    chest = models.FloatField()
    waist = models.FloatField()
    hip = models.FloatField()
    calf = models.FloatField()
    biceps = models.FloatField()

class Workout(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    conditin_before = models.TextField(max_length=1000)
    condition_during = models.TextField(max_length=1000)
    condition_after = models.TextField(max_length=1000)

class Exercise(models.Model):
    name = models.CharField(max_length=100)
    group_muscul = models.CharField(max_length=30)
    type_exercise = models.CharField(max_length=30)
    description = models.TextField(max_length=1000)
    wiki = models.URLField(max_length=300)

class ExerciseSmall(models.Model):
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE)
    nubmer = models.SmallIntegerField()
    workout = models.ForeignKey(Workout, on_delete=models.CASCADE, related_name='exercisesmall')


class Work(models.Model):
    weigth = models.SmallIntegerField()
    repeat = models.SmallIntegerField()
    sets = models.SmallIntegerField()
    exercise = models.ForeignKey(ExerciseSmall, on_delete=models.CASCADE, related_name='work')



