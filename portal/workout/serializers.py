from rest_framework import serializers
from .models import Workout, ExerciseSmall, Exercise, Work, Proportion

class WorkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Work
        fields = ('id', 'weigth', 'repeat', 'sets')


class ExerciseSmallSerializer(serializers.ModelSerializer):
    work = WorkSerializer(many=True)
    class Meta:
        model = ExerciseSmall
        fields = ('id', 'exercise', 'nubmer', 'work')

class WorkoutSerializer(serializers.ModelSerializer):
    exercisesmall = ExerciseSmallSerializer(many=True)
    class Meta:
        model = Workout
        fields = ('id', 'date', 'conditin_before', 'condition_during', 'condition_after', 'exercisesmall')


class ExerciseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Exercise
        fields = ('id', 'name', 'group_muscul', 'type_exercise', 'description', 'wiki')


class ProportionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Proportion
        fields = ('id', 'date', 'weight', 'neck', 'chest', 'biceps', 'waist', 'hip', 'calf')