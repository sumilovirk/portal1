from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    url(r'^pro/$', views.ProportionList.as_view()),
    url(r'^pro/(?P<pk>[0-9]+)/$', views.ProportionDetail.as_view()),
    url(r'^work/$', views.WorkoutList.as_view()),
    url(r'^work/(?P<pk>[0-9]+)/$', views.WorkoutDetail.as_view()),
    url(r'^exer/$', views.ExerciseList.as_view()),
    url(r'^exer/(?P<pk>[0-9]+)/$', views.ExerciseDetail.as_view()),
    url(r'^exers/$', views.ExerciseSmallList.as_view()),
    url(r'^exers/(?P<pk>[0-9]+)/$', views.ExerciseSmallDetail.as_view()),
]

#urlpatterns = format_suffix_patterns(urlpatterns)