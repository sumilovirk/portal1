# Generated by Django 2.0.9 on 2018-11-14 08:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Exercise',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('group_muscul', models.CharField(max_length=30)),
                ('type_exercise', models.CharField(max_length=30)),
                ('description', models.TextField(max_length=1000)),
                ('wiki', models.URLField(max_length=300)),
            ],
        ),
        migrations.CreateModel(
            name='ExerciseSmall',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nubmer', models.SmallIntegerField()),
                ('exercise', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='workout.Exercise')),
            ],
        ),
        migrations.CreateModel(
            name='Proportion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('weight', models.FloatField()),
                ('neck', models.FloatField()),
                ('chest', models.FloatField()),
                ('waist', models.FloatField()),
                ('hip', models.FloatField()),
                ('calf', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Work',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('weigth', models.SmallIntegerField()),
                ('repeat', models.SmallIntegerField()),
                ('sets', models.SmallIntegerField()),
                ('exercise', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='work', to='workout.ExerciseSmall')),
            ],
        ),
        migrations.CreateModel(
            name='Workout',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('conditin_before', models.TextField(max_length=1000)),
                ('condition_during', models.TextField(max_length=1000)),
                ('condition_after', models.TextField(max_length=1000)),
            ],
        ),
        migrations.AddField(
            model_name='exercisesmall',
            name='workout',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='exercisesmall', to='workout.Workout'),
        ),
    ]
