from django.apps import AppConfig


class WorkoutConfig(AppConfig):
    name = "portal.workout"
    verbose_name = "workout"