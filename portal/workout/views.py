from django.shortcuts import render
from rest_framework import generics

from .models import Proportion, Workout, Exercise, ExerciseSmall, Work
from .serializers import ProportionSerializer, WorkoutSerializer, ExerciseSerializer, ExerciseSmallSerializer,WorkSerializer


'''
Proportion
'''
class ProportionList(generics.ListCreateAPIView):
    queryset = Proportion.objects.all()
    serializer_class = ProportionSerializer


class ProportionDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Proportion.objects.all()
    serializer_class = ProportionSerializer


'''
Workout
'''
class WorkoutList(generics.ListCreateAPIView):
    queryset = Workout.objects.all()
    serializer_class = WorkoutSerializer


class WorkoutDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Workout.objects.all()
    serializer_class = WorkoutSerializer


'''
Exercise
'''
class ExerciseList(generics.ListCreateAPIView):
    queryset = Exercise.objects.all()
    serializer_class = ExerciseSerializer


class ExerciseDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Exercise.objects.all()
    serializer_class = ExerciseSerializer


'''
ExerciseSmall
'''
class ExerciseSmallList(generics.ListCreateAPIView):
    queryset = ExerciseSmall.objects.all()
    serializer_class = ExerciseSmallSerializer


class ExerciseSmallDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ExerciseSmall.objects.all()
    serializer_class = ExerciseSmallSerializer
